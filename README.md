
# Use Technologies below:

# Backend server

## All of the bellow technologies are used --

Spring Boot 2

Spring Security


Spring Data

Hibernate 5 + JPA 2

H2 Or Postgresql Database 


Lombok

Maven

# Build & run  Maven Spring Boot Applications 

Project name : UrlShortenerService

goto where pom.xml is keep

`mvn install`

`java -jar target/UrlShortenerService.jar`

`mvn spring-boot:run`


# Swagger API Docs

When you run our application, specification will be generated. You can check it here:

### http://localhost:6969/swagger-ui.html#

###curl Test

curl -X POST "http://localhost:6969/api/generate" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"longUrl\": \"http://stackoverflow.com\", \"params\": { \"paramA\": \"value1\", \"paramB\": \"value2\", \"paramC\": \"value3\"}, \"shortUrlDomain\": \"localhost:6969\"}"

curl -X GET "http://localhost:6969/api/clickedData/list" -H "accept: application/json"

curl -X GET "http://localhost:6969/api/getSingleUrl/{id}" -H "accept: application/json"

curl -X GET "http://localhost:6969/api/{urlcode}" -H "accept: application/json"

curl -X GET "http://localhost:6969/api/totalCount" -H "accept: application/json"
