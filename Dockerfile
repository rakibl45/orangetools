FROM maven:3.6.3-jdk-11 AS builder
WORKDIR /opt/app
ADD . /opt/app


VOLUME /tmp

ADD target/UrlShortenerService.jar UrlShortenerService.jar
EXPOSE 8585
ENTRYPOINT ["java", "-jar", "UrlShortenerService.jar"]