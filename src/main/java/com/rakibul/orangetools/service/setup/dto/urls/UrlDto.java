package com.rakibul.orangetools.service.setup.dto.urls;

import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rakibul
 */

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlDto {

    @NotNull(message= "longUrl value can not be null ")
    private String longUrl;

    @NotNull(message= "shortUrlDomain value can not be null ")
    private String shortUrlDomain;

    private  Map<String, Object> params;

}
