package com.rakibul.orangetools.service.setup.service.impl;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;

import javax.validation.ValidationException;

import com.rakibul.orangetools.service.constant.AppConstant;
import com.rakibul.orangetools.service.setup.domain.Url;
import com.rakibul.orangetools.service.setup.dto.urls.UrlDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlListDto;
import com.rakibul.orangetools.service.setup.repository.UrlRepository;
import com.rakibul.orangetools.service.setup.service.IUrlShortenerService;
import com.rakibul.orangetools.service.utility.Utils;
import org.modelmapper.TypeToken;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UrlShortenerServiceImpl implements IUrlShortenerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerServiceImpl.class);
	private static final Type URL_LIST = new TypeToken<List<UrlListDto>>() {
	}.getType();

	@Autowired
	UrlRepository urlRepository;

	@Autowired
	ModelMapper modelMapper;

    @Autowired
    Utils utils;

	@Override
	public List<UrlListDto> getAllClickedUrlShortenerList() {
		try {


			LOGGER.info("*************Start retrieve the process UrlShortener list *****************");
			Iterable<Url> clickedUrlLists = urlRepository.findAllClickedUrl();
			LOGGER.info("*************Process completed*****************");

			List<UrlListDto> urlList = modelMapper.map(clickedUrlLists, URL_LIST);
			return urlList;
		} catch (Exception ex) {
			LOGGER.error("error occurd while retrive UrlShortener data::" + ex);
		}
		return null;
	}


    @Override
    public Url generateShortLink(UrlDto urlDto) {

        if(StringUtils.isNotEmpty(urlDto.getLongUrl()))
        {
            if(!Utils.isValid(urlDto.getLongUrl())) {
                throw new ValidationException(": "+urlDto.getLongUrl() + " : is not a valid url ");
            }

            Url urlToPersist = new Url();

            urlToPersist.setLongUrl(urlDto.getLongUrl());
            String urlShortCode = utils.generateUniqueString();
            String modifiedShortUrl = "http://" + urlDto.getShortUrlDomain()  + "/api/" + urlShortCode;
            urlToPersist.setShortUrlDomain(urlDto.getShortUrlDomain());
            urlToPersist.setShortUrl(modifiedShortUrl);
            urlToPersist.setUrlCode(urlShortCode);
            urlToPersist.setParams(urlDto.getParams());
            Url urlToRet = persistShortLink(urlToPersist);

            if(urlToRet != null)
                return urlToRet;

            return null;
        }
        return null;
    }


    @Override
    public Url persistShortLink(Url url) {
        Url urlToRet = urlRepository.save(url);
        return urlToRet;
    }

    @Override
    public Url getShortUrl(String url) {
        Url urlToRet = urlRepository.findByShortUrl(url);
        return urlToRet;
    }


    @Override
    public UrlListDto getUrlsById(UUID id) {
        UrlListDto urlDto = null;
        try {
            LOGGER.info("*************Start retrieve the process url *****************");
            Url url = urlRepository.findById(id).orElse(null);
            LOGGER.info("*************Process completed*****************");
            urlDto = modelMapper.map(url, UrlListDto.class);
        } catch (Exception ex) {
            LOGGER.error("An error occurred while retiving url" + ex.getMessage());

        }
        return urlDto;
    }


}
