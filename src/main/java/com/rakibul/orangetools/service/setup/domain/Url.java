package com.rakibul.orangetools.service.setup.domain;

import java.util.Map;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.rakibul.orangetools.service.utility.HashMapConverter;


import lombok.Getter;
import lombok.Setter;
/**
 *
 * @author rakibul
 */
@Entity
@Table(name = "urls")
@Setter
@Getter
public class Url {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="url_id",  nullable = false)
    private UUID Id;

    @Column(name="long_url",  nullable = false)
    private String longUrl;

    @Column(name="short_url_domain",  nullable = false)
    private String shortUrlDomain;

    @Column(name="short_url",  nullable = false)
    private String shortUrl;

    @Column(name="url_code",  nullable = false)
    private String urlCode;

    @Column(name="params")
    @Convert(converter = HashMapConverter.class)
    private  Map<String, Object> params;
        
    @Column(name="device_details")
    private String deviceDetails;

    @Column(name="ip_address")
    private String ipAddress;

    @Column(name="is_clicked", columnDefinition = "boolean default false")
    private boolean isClicked;

}