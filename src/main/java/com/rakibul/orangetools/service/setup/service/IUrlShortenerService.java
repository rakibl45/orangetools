package com.rakibul.orangetools.service.setup.service;

import java.util.List;
import java.util.UUID;

import com.rakibul.orangetools.service.setup.domain.Url;
import com.rakibul.orangetools.service.setup.dto.urls.UrlDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlListDto;



public interface IUrlShortenerService {
	
    public Url generateShortLink(UrlDto urlDto);
    public Url persistShortLink(Url url);
    public Url getShortUrl(String url);
    List<UrlListDto> getAllClickedUrlShortenerList();
    public UrlListDto getUrlsById(UUID id);
    

}
