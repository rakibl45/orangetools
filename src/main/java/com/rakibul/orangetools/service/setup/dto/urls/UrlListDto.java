package com.rakibul.orangetools.service.setup.dto.urls;

import java.util.Map;
import java.util.UUID;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rakibul
 */

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlListDto {
    private UUID Id;

    private String longUrl;

    private String shortUrlDomain;

    private String shortUrl;

    private String urlCode;
    
    private  Map<String, Object> params;
   
    private String deviceDetails;

    private String ipAddress;

    private boolean isClicked;
}
