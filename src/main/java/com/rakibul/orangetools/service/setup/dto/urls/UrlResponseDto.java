package com.rakibul.orangetools.service.setup.dto.urls;

import java.util.UUID;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rakibul
 */

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlResponseDto {
    private UUID Id;
    private String shortUrlDomain;

}
