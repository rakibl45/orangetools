package com.rakibul.orangetools.service.setup.dto.urls;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author rakibul
 */

@Setter
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UrlErrorResponseDto {
    private String status;
    private String error;

}
