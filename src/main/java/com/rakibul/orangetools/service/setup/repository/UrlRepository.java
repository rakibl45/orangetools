package com.rakibul.orangetools.service.setup.repository;

import java.util.List;
import java.util.UUID;

import com.rakibul.orangetools.service.setup.domain.Url;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by Rakibul
 */
public interface UrlRepository extends JpaRepository<Url, UUID>  {

	@Query(value = "SELECT * FROM urls WHERE url_code = :url ", nativeQuery = true)
	Url findByShortUrl(@Param("url") String url);

	@Query(value = "SELECT * FROM urls WHERE is_clicked = true", nativeQuery = true)
	List<Url> findAllClickedUrl();

}
