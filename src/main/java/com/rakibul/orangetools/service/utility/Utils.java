package com.rakibul.orangetools.service.utility;

import static java.util.Objects.nonNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Objects;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ua_parser.Client;
import ua_parser.Parser;

@Component
public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    private static final String UNKNOWN = "UNKNOWN";

    public static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("UnsupportedEncoding error", e);
            throw new UnsupportedOperationException(e);
        }
    }

    public static boolean isValid(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            LOGGER.error("URL reading error", e);
            return false;
        }
    }

    public static String extractIp(HttpServletRequest request) {
        String clientIp;
        String clientXForwardedForIp = request.getHeader("x-forwarded-for");
        if (nonNull(clientXForwardedForIp)) {
            clientIp = parseXForwardedHeader(clientXForwardedForIp);
        } else {
            clientIp = request.getRemoteAddr();
        }
        return clientIp;
    }

    private static String parseXForwardedHeader(String header) {
        return header.split(" *, *")[0];
    }

    public static String getDeviceDetails(String userAgent) throws IOException {
        String deviceDetails = UNKNOWN;
        Parser parser = new Parser();

        Client client = parser.parse(userAgent);

        if (Objects.nonNull(client)) {
            deviceDetails = client.userAgent.family + " " + client.userAgent.major + "." + client.userAgent.minor
                    + " - " + client.os.family + " " + client.os.major + "." + client.os.minor;
        }

        return deviceDetails;
    }

    public String generateUniqueString() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(0,5);
    }


}