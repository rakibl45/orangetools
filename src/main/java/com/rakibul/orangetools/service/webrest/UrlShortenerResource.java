package com.rakibul.orangetools.service.webrest;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.rakibul.orangetools.service.constant.AppConstant;
import com.rakibul.orangetools.service.setup.domain.Url;
import com.rakibul.orangetools.service.setup.dto.urls.TotalDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlErrorResponseDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlListDto;
import com.rakibul.orangetools.service.setup.dto.urls.UrlResponseDto;
import com.rakibul.orangetools.service.setup.repository.UrlRepository;
import com.rakibul.orangetools.service.setup.service.IUrlShortenerService;
import com.rakibul.orangetools.service.utility.AppResponse;
import com.rakibul.orangetools.service.utility.Utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api", produces = { "application/json" })
public class UrlShortenerResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerResource.class);
    public String queryparms;

    @Autowired
    IUrlShortenerService urlShortenerService;

    @Autowired
    UrlRepository urlRepo;

    @PostMapping("/generate")
    public ResponseEntity<?> generateShortLink(@Valid @RequestBody UrlDto urlDto) {

        Url urlToRet = urlShortenerService.generateShortLink(urlDto);

        if (urlToRet != null) {
            UrlResponseDto urlResponseDto = new UrlResponseDto();
            urlResponseDto.setId(urlToRet.getId());
            urlResponseDto.setShortUrlDomain(urlToRet.getShortUrlDomain());
            return new ResponseEntity<UrlResponseDto>(urlResponseDto, HttpStatus.OK);
        }
        UrlErrorResponseDto urlErrorResponseDto = new UrlErrorResponseDto();
        urlErrorResponseDto.setStatus("404");
        urlErrorResponseDto.setError("There was an error processing your request. please try again.");
        return new ResponseEntity<UrlErrorResponseDto>(urlErrorResponseDto, HttpStatus.OK);

    }

    @GetMapping("/getSingleUrl/{id}")
    ResponseEntity<?> getUrlsById(@PathVariable(name = "id") UUID id) {
        try {
            if (id == null || StringUtils.isBlank(id.toString())) {
                return new ResponseEntity<Object>(AppResponse.isBadRequest(), new HttpHeaders(),
                        HttpStatus.BAD_REQUEST);
            }
            UrlListDto url = urlShortenerService.getUrlsById(id);
            return new ResponseEntity<Object>(url, new HttpHeaders(), HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.info("An error occurred during retrieved data ::" + e);
            return new ResponseEntity<Object>(AppResponse.isInternalServerError(), new HttpHeaders(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{urlCode}")
    public ResponseEntity<?> redirectToOriginalUrl(@PathVariable(name = "urlCode") String shortLink,
            HttpServletResponse response, HttpServletRequest request) throws IOException {
        String ip = Utils.extractIp(request);
        String deviceDetails = Utils.getDeviceDetails(request.getHeader("user-agent"));

        if (StringUtils.isEmpty(shortLink)) {
            UrlErrorResponseDto urlErrorResponseDto = new UrlErrorResponseDto();
            urlErrorResponseDto.setError("Invalid Url");
            urlErrorResponseDto.setStatus("400");
            return new ResponseEntity<UrlErrorResponseDto>(urlErrorResponseDto, HttpStatus.OK);
        }

        Url urlToRet = urlShortenerService.getShortUrl(shortLink);
        if (urlToRet == null) {
            UrlErrorResponseDto urlErrorResponseDto = new UrlErrorResponseDto();
            urlErrorResponseDto.setError("Url does not exist !");
            urlErrorResponseDto.setStatus("400");
            return new ResponseEntity<UrlErrorResponseDto>(urlErrorResponseDto, HttpStatus.OK);
        } else {

            urlToRet.setIpAddress(ip);
            urlToRet.setDeviceDetails(deviceDetails);
            urlToRet.setClicked(true);
            urlRepo.save(urlToRet);
        }

        if (urlToRet.getParams() != null) {

            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, Object> entry : urlToRet.getParams().entrySet()) {
                if (sb.length() > 0) {
                    sb.append("&");
                }
                sb.append(String.format("%s=%s", Utils.urlEncodeUTF8(entry.getKey().toString()),
                        Utils.urlEncodeUTF8(entry.getValue().toString())));
            }
            queryparms = sb.toString();
        }
        if (queryparms != null) {
            response.sendRedirect(urlToRet.getLongUrl().replaceFirst("/*$", "") + "? " + queryparms);
        } else {
            response.sendRedirect(urlToRet.getLongUrl());
        }

        return null;
    }

    @GetMapping("/clickedData/list")
    ResponseEntity<?> getAllClickedUrlShortenerList() {
        try {
            List<UrlListDto> list = urlShortenerService.getAllClickedUrlShortenerList();
            return new ResponseEntity<Object>(list, new HttpHeaders(), HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.info("An error occurred during retrieved data ::" + e);
            return new ResponseEntity<Object>(AppResponse.isInternalServerError(), new HttpHeaders(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/totalCount")
    ResponseEntity<?> getTotal() {
        try {
            int total = urlRepo.findAll().size();
            TotalDto totalCount = new TotalDto();
            totalCount.setTotalCount(total);
            return new ResponseEntity<Object>(totalCount, new HttpHeaders(), HttpStatus.OK);

        } catch (Exception e) {
            LOGGER.info("An error occurred during retrieved data ::" + e);
            return new ResponseEntity<Object>(AppResponse.isInternalServerError(), new HttpHeaders(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
